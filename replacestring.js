let string = 'Welcome to new world'
let newArray = []

function replaceString(str, replaceString, newString) {
    const splitString = str.split('')
    const length = splitString.length - 1
    let newArray = []
    for(let i = 0; i <= length; i++) {
        if(splitString[i] === replaceString) {
            newArray.push(newString)
        } else {
            newArray.push(splitString[i])
        }
    }
    const result = newArray.join('')
    console.log(result)
}

replaceString(string, 'l', 'r')
