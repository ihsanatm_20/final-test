let stringOne = 'fried'
let stringTwo = 'fired'

function anagram(arg1, arg2) {
    let a = arg1.split('').sort().join('')
    let b = arg2.split('').sort().join('')
    if(a === b) {
        console.log('anagram')
    } else {
        console.log('not anagram')
    }
    return console.log(a + ' ' + b)
}

anagram(stringOne, stringTwo)